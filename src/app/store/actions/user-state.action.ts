import { createAction } from "@ngrx/store";

export const loginUser = createAction('[User] Login')
export const logoutUser = createAction('[User] Logout')